// var url = helpers.getUrl();

angular.module(MODULE_NAME)
.service('loginService', ['$http', function($http) {
  var produccion = false
  var url
  if (produccion) {
    url = 'http://www.hanser.com.mx'
  } else {
    url = 'http://localhost:3003'
  }
  console.log(url);
  var urlBase = url + '/login';

  this.getAll = function() {
    return $http.get(urlBase + '/all');
  }

  this.getFindUser = function(d) {
    console.log('service');
    return $http.post(urlBase + '/buscar-usuario' , d);
  }

  this.cerrarSesion = function() {
    return $http.post(urlBase);
  }

  this.registrarUsuario = function(d) {
    return $http.post(urlBase + '/registro', d)
  }

  this.subirFoto = function(d) {
    console.log(d,'***********service');
    return $http.post(urlBase + '/subir', d, {
           transformRequest: angular.identity,
           headers: {'Content-Type': undefined}
        })
  }

  this.guardarFoto = function(d) {
    return $http.post(urlBase + '/guardarFoto', d)
  }


  // this.setNew = function(d) {
  //   return $http.post(urlBase + '/test',d);
  // }

}]);
