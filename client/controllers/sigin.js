require('../services/sigin');
var sha256 = require('js-sha256');

angular.module(MODULE_NAME)
.controller('siginCtrl', ['$scope', 'siginService', '$timeout', function($scope, siginService, $timeout) {
  var ctrl = this;
  $scope.hola = "new life"

$scope.init = init;
$scope.btnRegistrarse = btnRegistrarse;
$scope.validar_email = validar_email;

$scope.loading = true
$scope.sigin = {}

// $scope.btnModalSubir = btnModalSubir;
// $('.fileinput').fileinput()


function init(d) {
  var mySesion = JSON.parse(d);
  console.log(mySesion,'***********');
  $scope.loading = false
}

function validar_email(email) {
    var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email) ? true : false;
}

function btnRegistrarse(){
  $scope.loading = true
  console.log($scope.sigin);
  if ($scope.sigin.usuario === undefined ||
      $scope.sigin.correo === undefined  ||
      $scope.sigin.password === undefined ||
      $scope.sigin.passwordx2 === undefined ||
      $scope.sigin.usuario === '' ||
      $scope.sigin.correo === ''  ||
      $scope.sigin.password === '' ||
      $scope.sigin.passwordx2 === '') {
        $scope.loading = false
        alertify.error('Todos los campos son requeridos');
  }else {
    if(validar_email( $scope.sigin.correo ) ){
      $scope.correoValidar = false
      console.log("correo bien");
    }else {
      $scope.correoValidar = true
      $scope.loading = false
      alertify.error('Correo incorrecto');
    }
    if ($scope.sigin.password === $scope.sigin.passwordx2) {
        $scope.passwordValidar = false
        console.log("contraseña bien");
    }else {
      $scope.passwordValidar = true
      $scope.loading = false
      alertify.error('Las contraseñas no coinciden');
    }
    if ($scope.correoValidar === false && $scope.passwordValidar === false) {
      var d = {usuario: $scope.sigin.usuario}
      siginService.validarUsuario(d)
      .success(function(res){
        if (res.err === false) {
          $scope.loading = false
          $scope.sigin = {}
          alertify.error('El usuario ya existe');
        }else {
          var d = {usuario: $scope.sigin.usuario, password: sha256($scope.sigin.password), correo: $scope.sigin.correo}
          siginService.registrarUsuario(d)
          .success(function(res){
            console.log(res);
            $scope.sigin = {}
            $scope.loading = false
            location.href='/login'
          })
        }
      })
    }
  }
}


}]);

    angular.module(MODULE_NAME)
    .directive('fileModel', ['$parse', function ($parse) {
        return {
           restrict: 'A',
           link: function(scope, element, attrs) {
              var model = $parse(attrs.fileModel);
              var modelSetter = model.assign;
              element.bind('change', function(){
                 scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                 });
              });
           }
        };
     }]);
