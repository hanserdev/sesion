require('../services/login');
var sha256 = require('js-sha256');

angular.module(MODULE_NAME)
.controller('loginCtrl', ['$scope', 'loginService', '$timeout', function($scope, loginService, $timeout) {
  var ctrl = this;
  $scope.hola = "new life"

$scope.init = init;
$scope.btnIniciarSesion = btnIniciarSesion;
$scope.btnCerrarSesion = btnCerrarSesion;

$scope.loading = true
$scope.login = {}

// $scope.btnModalSubir = btnModalSubir;
// $('.fileinput').fileinput()


function init(d) {
  var mySesion = JSON.parse(d);
  console.log(mySesion,'***********');
  $scope.loading = false
}

function btnIniciarSesion(){
  $scope.loading = true
  if ($scope.login.usuario === undefined || $scope.login.password === undefined) {
    $scope.loading = false
    alertify.error('Todos los campos son requeridos');
  }else {
    var usuario = $scope.login.usuario
    var d = {usuario: $scope.login.usuario, password: sha256($scope.login.password)}
    console.log(d);
    loginService.getFindUser(d)
    .success(function(res){
      console.log(res.length);
      if (res.result.length === 0) {
        $scope.loading = false
        alertify.error('Los campos ingresados no son correctos');
      }else {
        console.log(res);
        $scope.loading = false
        alertify.success('Bienvenido ' + usuario);
        setTimeout(function () {
          location.href='/inicio'
        }, 1000);
      }
    })
  }
}

function btnCerrarSesion(){
  $scope.loading = true
  loginService.cerrarSesion()
  .success(function(res){
    console.log(res);
    $scope.loading = false
    location.reload()
  })
}

}]);

    angular.module(MODULE_NAME)
    .directive('fileModel', ['$parse', function ($parse) {
        return {
           restrict: 'A',
           link: function(scope, element, attrs) {
              var model = $parse(attrs.fileModel);
              var modelSetter = model.assign;
              element.bind('change', function(){
                 scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                 });
              });
           }
        };
     }]);
