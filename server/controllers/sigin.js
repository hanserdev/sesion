const Model = require('../models/sigin');

module.exports = {
  getAll: getAll,
  getFindUser: getFindUser,
  cerrarSesion: cerrarSesion,
  registrarUsuario: registrarUsuario,
  subirFoto: subirFoto,
  guardarFoto: guardarFoto,
  validarUsuario: validarUsuario
}


function getAll (d) {
  return new Promise(function(resolve, reject) {
    Model.getAll(d)
    .then(function (res) {
      resolve(res);
    })
  });
}

function getFindUser (d) {
  return new Promise(function(resolve, reject) {
      Model.getFindUser(d)
      .then(function (res) {
        resolve(res)
      })
  });
}

function cerrarSesion (d) {
  return new Promise(function(resolve, reject) {
      Model.cerrarSesion(d)
      .then(function (res) {
        resolve(res)
      })
  });
}

function registrarUsuario (d) {
  return new Promise(function(resolve, reject) {
      Model.registrarUsuario(d)
      .then(function (res) {
        resolve(res)
      })
  });
}

function subirFoto (d) {
  return new Promise(function(resolve, reject) {
      Model.subirFoto(d)
      .then(function (res) {
        resolve(res)
      })
  });
}

function guardarFoto (d) {
  return new Promise(function(resolve, reject) {
      Model.guardarFoto(d)
      .then(function (res) {
        resolve(res)
      })
  });
}

function validarUsuario (d) {
  return new Promise(function(resolve, reject) {
      Model.validarUsuario(d)
      .then(function (res) {
        resolve(res)
      })
  });
}
