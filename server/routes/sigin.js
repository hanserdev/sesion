var express = require('express');
var router = express.Router();
var Home = require('../controllers/sigin');
var fs = require('fs');
var ExpressBrute = require('express-brute');
var store = new ExpressBrute.MemoryStore();
var bruteforce = new ExpressBrute(store);
var multer = require('multer');
// METHOD'S
// GET

var storage = multer.diskStorage({
  destination: './public/uploads/',
  filename: function (req, file, cb) {
    console.log(file);
    cb(null, file.originalname)
  }
})

var upload = multer({ storage: storage })

router.get('/all', getAll);
router.post('/buscar-usuario', bruteforce.prevent, getFindUser);
router.post('/', cerrarSesion);
router.post('/registro', registrarUsuario);
router.post('/subir', upload.single('file'), subirFoto);
router.post('/guardarFoto', guardarFoto);
router.post('/validar-usuario', bruteforce.prevent, validarUsuario);

function getAll(req, res) {
  var d = req.body ? req.body : {}
  Home.getAll(d)
  .then(function (result) {
    res.json(result);
  })
}

function getFindUser(req, res) {
  var d = req.body
  Home.getFindUser(d)
  .then(function (result) {
    if (result.result.length > 0) {
      req.session.mySesion = result.result[0]
      res.json(result);
    } else {
      res.json(result);
    }
  })
}

function cerrarSesion(req, res) {
  var d = req.body
  Home.cerrarSesion(d)
  .then(function (result){
    req.session.mySesion = null
    res.json(result);
  })
}

function registrarUsuario(req, res) {
  var d = req.body
  Home.registrarUsuario(d)
  .then(function (result){
    res.json(result);
  })
}

function subirFoto(req, res) {
  res.json({err: false})
}

function guardarFoto(req, res) {
  var d = req.body
  Home.guardarFoto(d)
  .then(function (result){
    res.json(result);
  })
}

function validarUsuario(req, res) {
  var d = req.body
  Home.validarUsuario(d)
  .then(function (result){
    res.json(result);
  })
}

module.exports = router;
