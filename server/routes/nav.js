var express = require('express');
var router = express.Router();


router.get('/', function(req, res) {
  res.redirect('inicio')
});

var produccion = false
var url
if (produccion) {
  url = 'http://www.hanser.com.mx'
} else {
  url = 'http://localhost:3003'
}

router.get('/inicio', function(req, res) {
  var mySesion = false
  if (req.session.mySesion) {
    console.log(req.session.mySesion);
    var mySesion = true
  } else {
    var mySesion = false
  }
  res.render('inicio', {usuario:true, url, mySesion: mySesion})

});

router.get('/login', function(req, res) {
  var mySesion = false
  if (req.session.mySesion) {
    res.redirect('/inicio')
    var mySesion = true
  } else {
    var mySesion = false
    res.render('login', {usuario:true, url, mySesion: mySesion})
  }

});

router.get('/sigin', function(req, res) {
  var mySesion = false
  if (req.session.mySesion) {
    res.redirect('/inicio')
    var mySesion = true
  } else {
    var mySesion = false
    res.render('sigin', {usuario:true, url, mySesion: mySesion})
  }

});


module.exports = router;
