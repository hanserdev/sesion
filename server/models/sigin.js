var conn = require('../models/main')
var moment = require('moment')

module.exports = {
  getAll: getAll,
  getFindUser: getFindUser,
  cerrarSesion: cerrarSesion,
  registrarUsuario: registrarUsuario,
  guardarFoto: guardarFoto,
  validarUsuario: validarUsuario
}

function getAll(d) {
  return new Promise(function(resolve, reject) {
    var query = `
                  SELECT
                    *
                  FROM
                    tipo_galerias
                `
    var inputs = [];
    conn.query(query, inputs,function(err, result, fields){
      if (err) {
        resolve({err: true, description: err})
      } else {
        resolve({err: false, result: result})
      }
    })
  })
}

function getFindUser(d) {
  return new Promise(function(resolve, reject) {
    var query = `
                  SELECT
                    *
                  FROM
                    usuarios
                  WHERE
                    usuario = ?
                  AND
                    password = ?
                  AND
                    estatus = 1
                `
    var inputs = [d.usuario, d.password];
    conn.query(query, inputs,function(err, result, fields){
      if (err) {
        resolve({err: true, description: err, tipo: 'buscarUsuario'})
      } else {
        resolve({err: false, result: result, tipo: 'buscarUsuario'})
      }
    })
  })
}

function cerrarSesion(d) {
  return new Promise(function(resolve, reject) {
    var query = `
                  SELECT
                    *
                  FROM
                    usuarios
                `
    var inputs = [];
    conn.query(query, inputs,function(err, result, fields){
      if (err) {
        resolve({err: true, description: err})
      } else {
        resolve({err: false, result: result})
      }
    })
  })
}

function registrarUsuario(d) {
  return new Promise(function(resolve, reject) {
    var query = `
    insert into
          usuarios (usuario, contrasenia, correo, activo, fecha_alta, perfil_id)
    VALUES
          (?, ?, ?, true, now(), 2)
                `
    var inputs = [d.usuario, d.contraseña, d.correo];
    conn.query(query, inputs,function(err, result, fields){
      if (err) {
        resolve({err: true, description: err, tipo: 'registro'})
      } else {
        resolve({err: false, result: result, tipo: 'registro'})
      }
    })
  })
}

function guardarFoto(d) {
  return new Promise(function(resolve, reject) {
    var query = `
    INSERT INTO
      galeria (foto, nombre, contenido, activo, usuario_id, tipo_galeria_id)
    VALUES
      (?, ?, ?, ?, ?, ?);
                `
    var inputs = [d.foto, d.nombre, d.contenido, d.activo, d.usuario_id, d.tipo_galeria_id];
    conn.query(query, inputs,function(err, result, fields){
      if (err) {
        resolve({err: true, description: err, tipo: 'registro'})
      } else {
        resolve({err: false, result: result, tipo: 'registro'})
      }
    })
  })
}

function validarUsuario(d) {
  return new Promise(function(resolve, reject) {
    var query = `
                  SELECT
                    *
                  FROM
                    usuarios
                  WHERE
                    usuario = ?
                  AND
                    activo = 1
                `
    var inputs = [d.usuario];
    conn.query(query, inputs,function(err, result, fields){
      if (err) {
        resolve({err: true, description: err, tipo: 'buscarUsuario'})
      } else {
        resolve({err: false, result: result, tipo: 'buscarUsuario'})
      }
    })
  })
}
